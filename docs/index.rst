.. SemanticLayerTools documentation master file, created by
   sphinx-quickstart on Fri Sep 24 14:43:14 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SciCom documentation
====================

Simulating various aspects of scientific communication via Agent-based models.

The development was part of the research project `ModelSEN <https://modelsen.mpiwg-berlin.mpg.de>`_
Socio-epistemic networks: Modelling Historical Knowledge Processes,
part of Department I of the Max Planck Institute for the History of Science and
funded by the Federal Ministry of Education and Research, Germany (Grant No. 01 UG2131).

The work is continued at the department for Structural Changes of the Technosphere, Max Planck Institute of Geoanthropology, Jena.

.. image:: _static/bmbf.png

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   readme
   usingmesa
   authors
   license


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
